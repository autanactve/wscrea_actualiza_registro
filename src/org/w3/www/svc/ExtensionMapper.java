
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package org.w3.www.svc;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "TablaType".equals(typeName)){
                   
                            return  org.w3.www.gis.TablaType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "WScrea_actualiza_registro-RQ-Type".equals(typeName)){
                   
                            return  org.w3.www.gis.WScrea_actualiza_registroRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "WScrea_actualiza_registro-RS-Type".equals(typeName)){
                   
                            return  org.w3.www.gis.WScrea_actualiza_registroRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "ListaRegistrosType".equals(typeName)){
                   
                            return  org.w3.www.gis.ListaRegistrosType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.w3.org/gis".equals(namespaceURI) &&
                  "ListaValoresType".equals(typeName)){
                   
                            return  org.w3.www.gis.ListaValoresType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    