
/**
 * WScrea_actualiza_registroSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package org.w3.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WScrea_actualiza_registroSkeleton java skeleton for the axisService
     */
    public class WScrea_actualiza_registroSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WScrea_actualiza_registroLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param listaRegistros
         */
        

                 public java.math.BigInteger creaactualizaregistro
                  (
                  org.w3.www.gis.ListaRegistrosType listaRegistros
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("listaRegistros",listaRegistros);
		try{
		
			return (java.math.BigInteger)
			this.makeStructuredRequest(serviceName, "creaactualizaregistro", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    